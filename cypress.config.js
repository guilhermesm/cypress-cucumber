
const { defineConfig } = require("cypress");
const cucumber = require("cypress-cucumber-preprocessor").default;
const { exec } = require('child_process');

module.exports = defineConfig({
  video: false,
  viewportWidth: 1280,
  viewportHeight: 875,
  chromeWebSecurity: false,
  numTestsKeptInMemory: 3,
  screenshotsFolder: "reports/screenshots",

  e2e: {
    setupNodeEvents(on, config) {
      on("file:preprocessor", cucumber());
      on("before:browser:launch", (browser = {}, launchOptions) => {
        if (browser.family === "chromium" && browser.name !== "electron") {
          launchOptions.args.push("--disable-dev-shm-usage");
        }
        return launchOptions;
      });

      on("after:run", async () => {
        console.log("after:run");
        console.log("**Gerando relatório HTML...**")
        exec('node ./cypress/support/reporter.js', (error, stdout, stderr) => {
          if (error) {
            console.error(`Ocorreu um erro: ${error}`);
            return;
          }
          console.log(`Saída do script: ${stdout}`);
         });
      });

    },
    excludeSpecPattern: "*.js",
    specPattern: "cypress/e2e/**/*.{feature, features}"
  },

});
