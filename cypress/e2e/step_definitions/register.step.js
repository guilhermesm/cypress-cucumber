/// <reference types="cypress" />

import { faker } from "@faker-js/faker";

import {
  Given,
  When,
  Then,
  But,
  Before,
} from "cypress-cucumber-preprocessor/steps";

/** Pré-requisitos :: Dado :: Given */
Given(`que esteja na tela de cadastro`, () => {
  cy.visit("https://front.serverest.dev/cadastrarusuarios");
});

Given(`que possua o primeiro usuário cadastrado na base`, () => {
  cy.request({
    method: "GET",
    url: "https://serverest.dev/usuarios",
  }).then((resposta) => {
    Cypress.env("email", resposta.body.usuarios[0].email);
    Cypress.env("senha", resposta.body.usuarios[0].password);
  });
});

Given(`que possua um usuário aleatório`, () => {
  /** Utilizando a biblioteca Faker, crio um usuário com dados aleatórios
   * e guardo este usuário como uma variável de ambiente utilizando Cypress.env
   */
  let usuario = {
    nome: faker.person.firstName(),
    email: faker.internet.email(),
    password: faker.internet.password(),
    administrador: "true",
  };

  Cypress.env("usuario", usuario);
});

/** Ações :: Quando :: When */
When(`cadastrar o usuário pelo front`, () => {
  /** Busco o usuário das variáveis de ambiente e salvo na variável usuario */
  let usuario = Cypress.env("usuario");

  /** Preencho os campos do formulário com os dados do usuário aleatório */
  cy.log("Passo não implementado!");
});

When(`cadastrar um usuário estando logado`, () => {
  /** Acesso a página de cadastro de usuários através do botão na página principal */

  /** Busco o usuário das variáveis de ambiente e salvo na variável usuario */

  /** Preencho os campos Nome, Email e senha com informações aleatórias*/

  /** O campo abaixo é um radio button e indica se o usuário cadastro é ou não administrador */
  cy.log("Passo não implementado!");
});

/** Validações :: Então :: Then */
Then(`deverá ser visualizado o usuário cadastrado na lista de usuários`, () => {
  /** Valido que foi redirecionado para a tela com a lista de usuários */

  /** Busco o usuário das variáveis de ambiente,
   * cadastrado no passo "que possua um usuário aleatório"
   */

  /** Valido que o usuário foi cadastrado e que suas propriedades estão na lista de usuarios.*/
  cy.log("Passo não implementado!");
});
